import { TodoModel } from './../../pages/shared/todo-model';
import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the DoneListPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'doneList',
  pure: false
})
export class DoneListPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(todos: TodoModel[]) {
    return todos.filter(todo => todo.check);
  }
}
