import { CreateItemPage } from './../create-item/create-item';
import { TodoModel } from './../shared/todo-model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

/**
 * Generated class for the TodoListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-todo-list',
  templateUrl: 'todo-list.html',
})
export class TodoListPage {
  public list: TodoModel[];
  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    public navParams: NavParams) {
    this.list = [
      new TodoModel("Test List", false),
      new TodoModel("Test List 2", true),
      new TodoModel("Test List 3", false),
      new TodoModel("Test List 4", false),
      new TodoModel("Test List 5", false, true),

    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TodoListPage');
  }

  goToOtherPage() {
    let modal = this.modalCtrl.create('CreateItemPage');
    modal.onDidDismiss(data => {
      if (data.description != "") {
        console.log(data);
        this.list.push(data);
      }
    })
    modal.present();

  }
  splitItem(item: TodoModel) {
    for (var i = 0; i < this.list.length; i++) {
      if (this.list[i].description == item.description) {
        this.list.splice(i, 1);
      }
    }
  }
  editItem(item, position) {
    let modal = this.modalCtrl.create('CreateItemPage', { item });
    modal.onDidDismiss(data => {
      console.log(data);
      if (data.description != undefined) {
        for (var i = 0; i < this.list.length; i++) {
          if (this.list[i].description == item.description) {
            this.list[i] = data;
          }
        }
      }
    })
    modal.present();
  }

}
