export class TodoModel {
    constructor(public description: string, public check?: boolean, public important?: boolean) {

    }

    static clone(todo: TodoModel) {
        return new TodoModel(todo.description, todo.check, todo.important);
    }

}