import { TodoModel } from './../shared/todo-model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the CreateItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-item',
  templateUrl: 'create-item.html',
})
export class CreateItemPage {
  public newItem = new TodoModel('');
  title: string = "Crear Item";

  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams) {
    if (this.navParams.get("item")) {
      console.log("Editando");
      console.log(this.navParams.get("item"));
      this.newItem = TodoModel.clone(this.navParams.get("item"));
      this.title = "Editar Item";
    }
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateItemPage');
  }
  createItem() {
    this.viewCtrl.dismiss(this.newItem);
  }
  closeModal() {
    this.viewCtrl.dismiss("Close view");
  }

}
