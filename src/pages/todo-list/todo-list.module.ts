import { PipesModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TodoListPage } from './todo-list';

@NgModule({
  declarations: [
    TodoListPage,
  ],
  imports: [
    IonicPageModule.forChild(TodoListPage),
    PipesModule
  ],
})
export class TodoListPageModule { }
