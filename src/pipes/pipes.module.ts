import { NgModule } from '@angular/core';
import { PrioridadPipe } from './../pipes/prioridad/prioridad';
import { DoneListPipe } from './../pipes/done-list/done-list';
@NgModule({
	declarations: [PrioridadPipe,
    DoneListPipe],
	imports: [],
	exports: [PrioridadPipe,
    DoneListPipe]
})
export class PipesModule {}
