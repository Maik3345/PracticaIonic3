import { TodoModel } from './../../pages/shared/todo-model';
import { Pipe, PipeTransform, Injectable } from '@angular/core';

/**
 * Generated class for the PrioridadPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'prioridadPipe',
  pure: false
})
@Injectable()
export class PrioridadPipe {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(todos: TodoModel[]) {
    return todos.filter(todo => !todo.check).sort((a, b) => (b.important && !a.important) ? 1 : -1);
  }
}
