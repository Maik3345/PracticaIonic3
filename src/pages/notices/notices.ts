import { ServicesProvider } from './../../providers/services/services';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

/**
 * Generated class for the NoticesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notices',
  templateUrl: 'notices.html',
})
export class NoticesPage {
  notices: any = [];
  key: string = "=1b13ca21cd1742c78f34609b939503a2";
  url: string = "https://newsapi.org/v1/articles?source=abc-news-au&sortBy=top&apiKey"
  constructor(public navCtrl: NavController,
    public API: ServicesProvider,
    public loadingCtrl: LoadingController,
    public navParams: NavParams) {
    this.getNotices();
  }
  getNotices(event?) {
    let loading = this.loadingCtrl.create();
    loading.present();

    // si el servidor se demora mucho en responder
    setTimeout(() => {
      // cierro loading generado
      loading.dismiss();
      // cierro loading de la vista
      if (event != undefined) {
        event.complete();
      }
    }, 2000);
    this.notices = [];
    this.API.getNotices(this.url + this.key).subscribe(res => {
      console.log(res);
      this.notices = res.articles;
      // cierro loading generado
      loading.dismiss();
      // cierro loading de la vista
      if (event != undefined) {
        event.complete();
      }
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad NoticesPage');
  }

}
