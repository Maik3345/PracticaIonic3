import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  LatLng,
  Marker
} from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
declare var google;
/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
  providers: [GoogleMaps, Geolocation]
})
export class MapPage {

  map: any;
  mapElement: HTMLElement;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private geolocation: Geolocation,
    public platform: Platform,
    public googleMap: GoogleMaps) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');
    this.getCurrentPosition();
  }

  getCurrentPosition() {
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log(resp);
      this.loadMap(resp);
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  loadMap(lat) {
    this.mapElement = document.getElementById('map');

    let myposition = new LatLng(lat.coords.latitude, lat.coords.longitude);
    // this.map.animateCamera({
    //   target: myposition,
    //   zoom: 10
    // })

    let myLatLng = new google.maps.LatLng({ lat: lat.coords.latitude, lng: lat.coords.longitude });

    this.map = new google.maps.Map(this.mapElement, {
      center: myLatLng,
      zoom: 12
    });
    // Creacion de markers en el mapa de acuerdo al objeto de datos de los usuarios
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      // this.map = this.googleMap.create(this.mapElement, mapOptions);
      let marker = new google.maps.Marker({
        position: myLatLng,
        map: this.map,
        animation: google.maps.Animation.DROP,
      });
      let content = `<h4>Hola</h4>`;

      this.addInfoWindow(marker, content);
    });

  }
  addInfoWindow(marker, content) {
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });

  }

}
